# ---------------------------------------- schwarzinvader-neocities ------------------------------------------
-----
Le site internet que j'ai crée sur neocities.org

-----
## Présentation
C'est un site statique écrit en HTML et CSS uniquement le Javascript viendra plus tard pour créer certaines animations. C'est un site personnel ne vous attendez pas à un design pro. Les choix seront fait uniquement selon mes goûts. Je vais y rajouter des truc au fur et à mesure de mes envies.

## Neocities
Neocities est un hébergeur qui propose un espace gratuit pour mettre en ligne des sites statiques. Il n'est pas possible de créer un site dynamique d'utiliser une base de donnée ou d'installer Wordpress ou  autre. Il y a diiférents type de sites, souvent des sites personnels avec des design beaucoup plus libres et diversifiés.
